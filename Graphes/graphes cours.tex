\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbf{R}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textsf{\textsc{\small{Démonstration}}}}
\newtheorem{exemple}{\textsf{\textsc{\small{Exemple}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}
\newtheorem{preuve}{Preuve}
\newtheorem{theoreme}{Théorème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{violet}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}

\title{Graphes}
\author{Term ES}
\date{}

\begin{document}


\maketitle\\
\section{Premi\`eres notions}

De très nombreux problèmes pratiques peuvent être ainsi schématisés à l'aide d'un graphe ; en simplifiant la représentation, on peut ainsi trouver plus rapidement la solution (ou en voir
l'impossibilité !). Pour certains problèmes, comme ceux que nous venons de voir, les arêtes n'ont pas d'orientation. Pour d'autres, il est indispensable d'avoir une orientation sur le graphe : le plan d'une ville comme graphe non orienté satisfera le piéton, tandis que ce même graphe orienté par les sens de circulation sera bien plus apprécié de l'automobiliste.

Une question importante est celle du choix du graphe associé à une situation donnée (il peut
y en avoir plusieurs) ; comment choisir les sommets et les arêtes ? Comme on vient de le voir
dans le problème des segments, ce n'est pas toujours évident. Dans des paragraphes ultérieurs, on étudiera des questions de compatibilité, il faudra décider si les arêtes correspondent aux couples de points compatibles ou incompatibles, et si les arêtes sont orientées ou non.

Nous allons formaliser les notions qui précèdent.


\begin{definition}[Graphe, sommets, arêtes, sommets adjacents] Un graphe $G$ (non orienté) est constitué d'un ensemble $S$ de points appelés \emph{sommets}, et d'un ensemble $A$ d'\emph{arêtes}, tels qu'à chaque arête sont associés deux sommets, appelés ses \emph{extrémités}.\\
Deux sommets qui sont les extrémités d'une arête sont dits \emph{adjacents}. \end{definition}


Les deux extrémités peuvent être distinctes ou confondues ; dans ce dernier cas, l'arête s'appelle une \emph{boucle}. Deux arêtes peuvent aussi avoir les mêmes extrémités (on dit alors qu'elles sont \emph{parallèles}). Cependant, la très grande majorité des problèmes que nous rencontrerons, où des graphes non orientés seront en jeu, concerne des graphes \emph{simples}, c'est-à-dire sans boucles ni arêtes parallèles. Les termes \emph{simples} et \emph{parallèles} ne sont pas à retenir.


\begin{exemple}  On considère le graphe $G_1$, de la figure suivante \\
Le sommet $s_4$ est un sommet isolé, l'arête $a$ est une boucle, $b$ et $c$ sont des arêtes ayant mêmes extrémités, les sommets $s_1$ et $s_2$ sont adjacents, ainsi que $s_1$ et $s_3$,
puisqu'ils sont reliés par une arête.
\end{exemple}
\begin{remarque} La position des sommets et la longueur ou l'allure des arêtes n'ont aucune importance. \end{remarque}
Une première manière d'évaluer la complication d'un graphe est de compter le nombre de ses
sommets :

\begin{definition}[Ordre d'un graphe] L'\emph{ordre} d'un graphe est le nombre de ses sommets.\end{definition}
\begin{definition}[Graphe simple] Un graphe est dit \emph{simple} si deux sommets distincts sont joints par au plus une arête et s'il est sans boucle.\end{definition}
\begin{definition}[Degré d'un sommet, parité d'un sommet] On appelle \emph{degré d'un sommet} le nombre d'arêtes dont ce sommet est une extrémité (les boucles étant comptées deux fois). Un sommet est \emph{pair} (respectivement \emph{impair}) si son degré est un nombre pair (respectivement impair). \end{definition}

\begin{exemple} Dans la figure , $s_1$ est de degré 5, $s_2$ de degré 3, $s_4$ de degré 0. \end{exemple}


On prouve facilement le théorème suivant :

\begin{theoreme} La somme des degrés de tous les sommets d'un graphe est égale à deux fois le nombre d'arêtes de ce graphe ; c'est donc un nombre pair. \end{theoreme}

\begin{preuve} Lorsque on additionne les degrés des sommets, chaque arête est comptée deux fois, une fois pour chaque extrémité. \end{preuve}

\begin{propriete} Dans un graphe le nombre de sommets impairs est toujours pair. \end{propriete}

\begin{preuve} En effet, sinon, la somme des degrés des sommets serait impaire. \end{preuve}

\begin{definition}[Graphe complet] Un graphe (simple) est dit \emph{complet} si tous ses sommets sont \emph{adjacents}, c'est-à-dire si toutes les arêtes possibles existent. On appelera $K_n$ le graphe complet à $n$ sommet (il n'y en a qu'un). \end{definition}


\begin{definition}[Sous-graphe, sous-graphe engendré par des sommets]
Soit $G$ un graphe, le graphe $G'$ est \underline{un} \emph{sous-graphe} de $G$, si :
\begin{itemize}
	\item l'ensemble des sommets de $G'$ est inclu dans celui des sommets de $G$ ;
	\item l'ensemble des arêtes de $G'$ est inclu dans celui des arêtes de $G$.
\end{itemize}
Si, de plus, les arêtes de $G'$ sont exactement \emph{\underline{toutes}} les arêtes de $G$ joignant les sommets de $G'$, on dit que $G'$ est \emph{\underline{le} sous-graphe de $G$ engendré par $s_1, s_2, \dots , s_n$}.
\end{definition}

\begin{exemple} Considérons le graphe $G$ dont les sommets sont les villes françaises possédant une gare et dont les arêtes sont les voies ferrées reliant ces villes (on excluera les gares où ne passent plus de voies).
\begin{itemize}
	\item Le graphe $G'$ dont les sommets sont les villes d'un même département possédant une gare et dont les arêtes sont les voies ferrées reliant ces villes est le sous-graphe de $G$ engendré par ces villes.
	\item Le graphe $G''$ dont les sommets sont les villes où passe un TGV et dont les arêtes sont des voies ferrées TGV est un simple sous-graphe de $G$ car il existe des voies normales reliant, par exemple, Marseille à Lyon, qui ne sont pas dans ce sous-graphe.
\end{itemize}
\end{exemple}

\begin{definition}[Sous-graphe stable] On dit qu'un sous-ensemble de l'ensemble des sommets est \emph{stable} s'il ne contient pas de paire de sommets adjacents.
On peut aussi parler de sous-graphe stable : cela revient au même, puisque si un ensemble de
sommets est stable, le graphe engendré, par définition, n'a pas d'arête. \end{definition}

\begin{exemple} Dans l'exercice ci-dessus, le sous-graphe $G_7$ est stable. \end{exemple}

Ce terme de \og stable \fg{} peut paraître arbitraire. Il est en fait naturel si l'on considère ce qu'on appelle un \og graphe d'incompatibilité \fg{} : dans un groupe d'individus, on peut définir un graphe en reliant par une arête les individus qui ne peuvent se supporter. Si l'on veut choisir un sous-groupe de personnes qui travaillent ensemble, il est préférable de choisir un sous-ensemble stable ! On verra en particulier beaucoup d'applications de cette notion dans le paragraphe sur les colorations.

\subsection{Chaînes et connexité}

Dans bien des problèmes de graphes, il est naturel de considérer ce que l'on peut appeler, de façon informelle, des \og parcours \fg{} ou \og chemins \fg. Le mot utilisé en théorie des graphes est \emph{chaîne}.

La notion intuitive de chaîne, ou plus tard de chaîne orientée, se comprend bien sur un dessin, il est moins facile d'en donner une définition effective.

\begin{definition}[Chaîne]
Une \emph{chaîne} dans un graphe $G$ est une suite finie : $s_0; a_1; s_1; a_2; s_2; a_3; s_3; \dots a_n; s_n$ débutant et finissant par un sommet, alternant sommets et arêtes de telle manière que chaque arête soit encadrée par ses sommets extrémités. \\
\end{definition}
\begin{definition}[Longueur chaîne]
La \emph{longueur} de la chaîne est égale au nombre d'arêtes qui la constituent.\end{definition}
\begin{definition}[Chaîne fermée] La chaîne est \emph{fermée} si $s_0 = s_n$,  l'origine et l'extrémité de la chaîne sont confondues.\end{definition}
\begin{definition}[Cycle]Si une chaîne est fermée et que de plus toutes ses arêtes sont distinctes on dit alors que c'est un \emph{cycle}.
\end{definition}

\begin{remarque} Quand il n'y a pas d'ambiguïté (pas d'arêtes multiples), on peut définir une chaîne
par seulement la suite de ses sommets ou par seulement la suite de ses arêtes. \end{remarque}

\begin{definition}[Graphe connexe] Un graphe est \emph{connexe} si deux sommets quelconques sont reliés par une chaîne. \end{definition}

\begin{definition}[Distance entre deux sommets] Soit $G$ un graphe connexe, $s$ et $s'$, deux sommets quelconques de $G$. Le graphe étant connexe, il existe au moins une chaîne reliant $s$ et $s'$. On appelle \emph{distance entre $s$ et $s'$} la plus petite des longueurs des chaînes reliant $s$ à $s'$. \end{definition}

\begin{remarque} Lorsque le graphe n'est pas connexe, il existe au moins deux sommets qui ne sont pas reliés par une chaîne. On dit parfois que la distance entre ces sommets est infinie.
\end{remarque}

\begin{definition}[Diamètre d'un graphe] On appelle \emph{diamètre} d'un graphe connexe, la plus grande distance entre ses sommets. \end{definition}

\begin{remarque} Lorsque le graphe n'est pas connexe, on dit parfois que son diamètre est infini.
\end{remarque}
\subsection{Graphes orientés}

Il existe de nombreux domaines où les graphes sont orientés. Par exemple : plan de ville, avec les
sens interdits ; parcours en montagne, où il est utile d'indiquer le sens de montée ! Circuit électrique
en courant continu, où il faut orienter les arêtes pour décider du signe de l'intensité : ce n'est pas
la même chose de faire passer 10 ampères de A vers B ou de B vers A ; graphe d'ordonnancement,
où les arêtes relient une tâche à une autre qui doit la suivre : on ne peut faire la peinture avant le
plâtre.

\begin{definition}
On appelle graphe orienté un graphe où chaque arête est orientée, c'est-à-dire
qu'elle va de l'une des ses extrémités, appelée \emph{origine} ou \emph{extrémité initiale} à l'autre, appelée
\emph{extrémité terminale}.
\end{definition}

Dans un graphe orienté, chaque arête orientée possède un début et une fin.
Toutes les notions que nous avons définies pour un graphe ont un équivalent pour un graphe
orienté. Nous nous contenterons de rajouter le mot \og orienté \fg{} pour préciser ; le contexte rendra
évidente l'interprétation à donner.

En particulier, une chaîne orientée est une suite d'arêtes telle que l'extrémité finale de chacune
soit l'extrémité initiale de la suivante. On prendra garde au fait que l'on peut définir et utiliser
des chaînes (non orientées) sur un graphe orienté. Par exemple, sur un plan de ville où toutes les
rues sont en sens unique, un parcours de voiture correspond à une chaîne orientée, un parcours de
piéton correspond à une chaîne (non orientée).

\section{Graphe eulérien}
\begin{definition}[Chaîne eulérienne] Une chaîne est \emph{eulérienne} si elle contient une fois et une seule chaque arête du graphe ; si la chaîne est un cycle (sommet de départ et de fin confondus), on l'appelle \emph{cycle eulérien}.
\end{definition}

Le théorème suivant, dit théorème d'\textsc{Euler}, qu'on admettra, est à l'origine de la théorie des graphes :

\begin{theoreme}[d'\textsc{Euler}]
Un graphe connexe a une chaîne eulérienne si et seulement si tous ses sommets sont pairs sauf au plus deux.
\end{theoreme}

De façon plus précise :
\begin{itemize}
\item si le graphe n'a pas de sommet impair, alors il admet une chaîne eulérienne ;
\item le graphe ne peut avoir un seul sommet impair ;
\item si le graphe a deux sommets impairs, ce sont les extrémités de la chaîne eulérienne.
\end{itemize}

La propriété suivante, conséquence immédiate du théorème, est souvent utile :

\begin{propriete}
Un graphe connexe ayant plus de deux sommets impairs ne possède pas de chaîne eulérienne.
\end{propriete}

Ces résultats permettent de résoudre beaucoup de problèmes pratiques se traitant en théorie
des graphes.


\end{document}