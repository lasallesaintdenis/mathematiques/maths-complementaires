\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3


\usepackage{tabularx}


\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}



\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbf{R}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother


\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}

\newtheorem{preuve}{Preuve}



% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Suites}
\author{TES}
\date{}

\begin{document}


\maketitle\\
\section{Suites géométriques}
\textcolor{blue}{\subsection{Définition}}
Dire qu'une suite $\left( u_n \right)$ est \emph{géométrique} signifie qu'il existe un nombre réel $q$ non nul tel que, pour tout entier $n$, \[u_{n+1} = q u_{n}\]

Le réel $q$ est appelé la raison de la suite géométrique.

\textcolor{teal}{\'Evolution en pourcentage}

\begin{itemize}
\item Augmenter une grandeur de $t\%$ équivaut à  multiplier sa valeur par $1 + \dfrac{t}{100}$.
\item Diminuer une grandeur de $t\%$ équivaut à  multiplier sa valeur par $1 - \dfrac{t}{100}$.
\end{itemize}

Chaque fois qu'on est confronté à une situation d'évolutions successives d'une grandeur de $t\%$, on peut définir une suite géométrique de raison $1 + \dfrac{t}{100}$ (augmentation) ou $1 - \dfrac{t}{100}$ (diminution)



Exemples : 
\begin{enumerate}
\item Un capital de 2000 est placé au taux d'intérêt composé de 1,5\% par an. 

On note $C_n$ le capital disponible au bout de $n$ années alors : \[C_{n+1}= \left( 1 +  \dfrac{1,5}{100}\right) \times C_n = 1,015 \times C_n\]

Ainsi, la suite $\left( C_n \right)$ est une suite géométrique de premier terme $C_0= 2000$ et de raison $q= 1,015$.

\item Pour lutter contre la pollution, un groupe industriel décide de réduire progressivement sa quantité de rejets de $4 \%$ par an. En 2012, la quantité de rejets était de \np{50000} tonnes.

On note $r_n$ la quantité de rejets l’année $2012+n$ d’où : \[r_{n+1}= \left( 1-  \dfrac{4}{100}\right) \times r_n = 0,96 \times r_n\]

Ainsi, la suite $\left( r_n \right)$ est une suite géométrique de premier terme $r_0=50000$ et de raison $0,96$.
\end{enumerate}

\textcolor{teal}{Propriété 1}

Soit  $\left( u_n \right)$ une suite géométrique de raison $q$ et de premier terme $u_0$ alors pour tout entier $n$, \[u_{n} = u_{0} \times q^n \]

Exemple : \\

L'objectif du groupe industriel est de réduire progressivement la quantité de rejets pour atteindre une quantité inférieure ou égale à  \np{30000} tonnes (soit une réduction de 40\%). Cet objectif sera-t-il atteint au bout de 10 ans ?

Au bout de 10 ans, la quantité de rejets est de : \[r_{10}= 50000\times 0,96^{10} \approx 33242\]

Avec un réduction de 4 \% par an, en 2022 l'objectif du groupe industriel ne sera pas atteint.

\textcolor{teal}{Propriété 2}

Si  $\left( u_n \right)$ une suite géométrique de raison $q$ alors pour tout entier $n$ et pour tout entier $p$, \[u_{n} = u_{p}\times q^{n-p}\]
\textcolor{blue}{\subsection{Monotonie}}

Soit  $\left( u_n \right)$ une suite géométrique de raison $q$ et de premier terme $u_0$ donc :
	 \[ \begin{split} 
u_{n+1} - u_n & = u_0 \times q^{n+1} -u_0 \times q^{n}\\
& = u_0 \times q^n \times  (q-1)\\
 	\end{split}
 	\]
La monotonie de la suite dépend du signe de $u_0$, $q^n$ et $(q-1)$  
\begin{itemize}
	\item Si $ q < 0$ alors $q^n$ est positif pour $n$ pair, négatif pour $n$ impair donc la suite n'est pas monotone.
	\item Si $ q > 0$ alors la suite est monotone, croissante ou décroissante selon le signe du produit $u_0 \times (q-1)$ .
\end{itemize}	

\begin{center}
Si $q>1$
\end{center}
\begin{multicols}{2}
Si $u_0 >0$ alors la suite $(u_n)$ est croissante.\\

\definecolor{qqqqff}{rgb}{0.,0.,1.}
\definecolor{cqcqcq}{rgb}{0.7529411764705882,0.7529411764705882,0.7529411764705882}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
\draw [color=cqcqcq,, xstep=1.0cm,ystep=1.0cm] (-0.46,-0.88) grid (7.7,4.56);
\draw[->,color=black] (-0.46,0.) -- (7.7,0.);
\foreach \x in {,1,2,3,4,5,6,7}
\draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[below] {\footnotesize $\x$};
\draw[->,color=black] (0.,-0.88) -- (0.,4.56);
\foreach \y in {,1.,2.,3.,4.}
\draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt);
\draw[color=black] (0pt,-10pt) node[right] {\footnotesize $0$};
\clip(-0.46,-0.88) rectangle (7.7,4.56);

\draw [fill=qqqqff] (0.,1.) circle (1.5pt);
\draw [fill=qqqqff] (1.02,1.2043837049448436) circle (1.5pt);
\draw [fill=qqqqff] (1.98,1.4347587009898446) circle (1.5pt);
\draw [fill=qqqqff] (2.98,1.7217104411878132) circle (1.5pt);
\draw [fill=qqqqff] (4.,2.0736) circle (1.5pt);
\draw [fill=qqqqff] (5.04,2.506533307896962) circle (1.5pt);
\draw [fill=qqqqff] (6.,2.9859839999999993) circle (1.5pt);

\end{tikzpicture}\\

\columnbreak
 Si $u_0 <0$ alors la suite $(u_n)$ est décroissante.\\

\definecolor{qqqqff}{rgb}{0.,0.,1.}
\definecolor{cqcqcq}{rgb}{0.7529411764705882,0.7529411764705882,0.7529411764705882}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
\draw [color=cqcqcq,, xstep=1.0cm,ystep=1.0cm] (-0.56,-7.4) grid (7.18,0.58);
\draw[->,color=black] (-0.56,0.) -- (7.18,0.);
\foreach \x in {,1,2,3,4,5,6,7}
\draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[below] {\footnotesize $\x$};
\draw[->,color=black] (0.,-7.4) -- (0.,0.58);
\foreach \y in {-7.,-6.,-5.,-4.,-3.,-2.,-1.}
\draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt);
\draw[color=black] (0pt,-10pt) node[right] {\footnotesize $0$};
\clip(-0.56,-7.4) rectangle (7.18,0.58);
\begin{scriptsize}
\draw [fill=qqqqff] (0.,-2.) circle (1.5pt);
\draw [fill=qqqqff] (1.02,-2.408767409889687) circle (1.5pt);
\draw [fill=qqqqff] (1.98,-2.869517401979689) circle (1.5pt);
\draw [fill=qqqqff] (2.98,-3.4434208823756265) circle (1.5pt);
\draw [fill=qqqqff] (4.,-4.1472) circle (1.5pt);
\draw [fill=qqqqff] (5.04,-5.013066615793924) circle (1.5pt);
\draw [fill=qqqqff] (6.,-5.971967999999999) circle (1.5pt);
\end{scriptsize}
\end{tikzpicture}

\end{multicols}



\begin{center}
Si $0<q<1$
\end{center}
\begin{multicols}{2}
Si $u_0>0$ alors la suite $(u_n)$ est décroissante.
\definecolor{qqqqff}{rgb}{0.,0.,1.}
\definecolor{cqcqcq}{rgb}{0.7529411764705882,0.7529411764705882,0.7529411764705882}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
\draw [color=cqcqcq,, xstep=1.0cm,ystep=0.5cm] (-0.76272495274647,-0.42436315492708204) grid (6.433207836398062,2.5027868356753444);
\draw[->,color=black] (-0.76272495274647,0.) -- (6.433207836398062,0.);
\foreach \x in {,1,2,3,4,5,6}
\draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[below] {\footnotesize $\x$};
\draw[->,color=black] (0.,-0.42436315492708204) -- (0.,2.5027868356753444);
\foreach \y in {,0.5,1.,1.5,2.,2.5}
\draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt);
\draw[color=black] (0pt,-10pt) node[right] {\footnotesize $0$};
\clip(-0.76272495274647,-0.42436315492708204) rectangle (6.433207836398062,2.5027868356753444);

\draw [fill=qqqqff] (0.,2.) circle (1.5pt);
\draw [fill=qqqqff] (1.02,0.9862327044933592) circle (1.5pt);
\draw [fill=qqqqff] (1.98,0.5069797398950145) circle (1.5pt);
\draw [fill=qqqqff] (3.,0.25) circle (1.5pt);
\draw [fill=qqqqff] (4.,0.125) circle (1.5pt);
\draw [fill=qqqqff] (5.,0.0625) circle (1.5pt);
\draw [fill=qqqqff] (6.,0.03125) circle (1.5pt);

\end{tikzpicture}\\

\columnbreak
Si $u_0<0$ alors la suite $(u_n)$ est croissante.
\definecolor{qqqqff}{rgb}{0.,0.,1.}
\definecolor{cqcqcq}{rgb}{0.7529411764705882,0.7529411764705882,0.7529411764705882}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
\draw [color=cqcqcq,, xstep=1.0cm,ystep=0.5cm] (-0.38971763754243377,-2.6692451026071384) grid (6.806215151602099,0.25790488799528805);
\draw[->,color=black] (-0.38971763754243377,0.) -- (6.806215151602099,0.);
\foreach \x in {,1,2,3,4,5,6}
\draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[below] {\footnotesize $\x$};
\draw[->,color=black] (0.,-2.6692451026071384) -- (0.,0.25790488799528805);
\foreach \y in {-2.5,-2.,-1.5,-1.,-0.5}
\draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt);
\draw[color=black] (0pt,-10pt) node[right] {\footnotesize $0$};
\clip(-0.38971763754243377,-2.6692451026071384) rectangle (6.806215151602099,0.25790488799528805);

\draw [fill=qqqqff] (0.,-2.) circle (1.5pt);
\draw [fill=qqqqff] (1.02,-0.9862327044933592) circle (1.5pt);
\draw [fill=qqqqff] (1.98,-0.5069797398950145) circle (1.5pt);
\draw [fill=qqqqff] (3.,-0.25) circle (1.5pt);
\draw [fill=qqqqff] (4.,-0.125) circle (1.5pt);
\draw [fill=qqqqff] (5.,-0.0625) circle (1.5pt);
\draw [fill=qqqqff] (6.,-0.03125) circle (1.5pt);

\end{tikzpicture}
\end{multicols}

\textcolor{teal}{Théorème 1}
Soit  $q$ un réel non nul. 
\begin{itemize}
	\item  Si  $ q < 0$ alors la suite $\left( q^n \right)$ n'est pas monotone.
	\item  Si  $ q > 1$ alors la suite $\left( q^n \right)$ est strictement croissante.
	\item  Si  $ 0< q < 1$ alors la suite $\left( q^n \right)$ est strictement décroissante.
 \item  Si  $ q = 1$ alors la suite $\left( q^n \right)$ est constante.
\end{itemize}

\textcolor{teal}{Théorème 2}
Soit  $\left( u_n \right)$ une suite géométrique de raison $q$ non nulle et de premier terme $u_0$ non nul
\begin{itemize}
	\item Si  $ q < 0$ alors la suite $\left( u_n \right)$ n'est pas monotone.
	\item Si  $ q > 0$ et  $ u_0 > 0$ alors la suite $\left( u_n \right)$ a le même sens de variation que la suite $\left( q^n \right)$.
	\item Si  $ q > 0$ et  $ u_0 < 0$ alors la suite $\left( u_n \right)$ a  le sens de variation contraire de celui de la suite $\left( q^n \right)$.
\end{itemize}

\textcolor{blue}{\subsection{Somme de termes consécutifs}}
Soit  $\left( u_n \right)$ une suite géométrique de raison $q \ne 1$ et de premier terme $u_0$ alors pour tout entier $n$, \[ u_0  +  u_{1}  + \cdots + u_n =\sum_{i=0}^{n} u_{i} = u_0 \left(\dfrac{1-q^{n+1}}{1-q}\right)\]

La somme $S$ de termes consécutifs d'une suite géométrique de raison $q \ne 1$  est :\[S= \text{premier terme } \times \frac{1- q^{\text{nombre de termes}}}{1-q}\]

\section{Limite d'une suite}

On étudie le comportement d'une suite $\left( u_n \right)$ quand $n$ prend de grandes valeurs. 

\textcolor{blue}{\subsection{Limite infinie}}
\textcolor{teal}{{définition}}\\

On dit qu'une suite $\left( u_n \right)$ admet une limite égale à   $+\infty$ quand $n$ tend vers  $+\infty$ si pour tout nombre réel $A$ strictement positif, tous les termes de la suite sont supérieurs à  $A$ à  partir d'un certain rang $p$. On écrit :\[ \displaystyle \lim_{n\to +\infty}u_n=+\infty \]

On dit qu'une suite $\left( u_n \right)$ admet une limite égale à  $-\infty$ quand $n$ tend vers  $+\infty$ si pour tout nombre réel $A$ strictement négatif, tous les termes de la suite sont inférieurs à  $A$ à  partir d'un certain rang $p$. On écrit :\[ \displaystyle \lim_{n\to +\infty}u_n=-\infty \]

\textcolor{blue}{\subsection{Limite finie}}
\textcolor{teal}{{définition}}\\
Soit $\left( u_n \right)$ une suite définie sur $\mathbb{N}$ et $\ell$ un réel. 
\begin{enumerate}
	\item Dire que la suite $\left( u_n \right)$ admet pour limite le réel $\ell$ signifie que tout intervalle ouvert de la forme $\left] \ell - r ; \ell +r \right[$ contient tous les termes de la suite à  partir d'un certain rang $p$. On écrit :\[ \displaystyle \lim_{n\to +\infty}u_n=\ell \]
	
	\item Une suite qui admet pour limite un réel $\ell$ est dite \emph{convergente}. 
	
\textcolor{teal}{{Propriété}}\\	
La suite  $\left( u_n \right)$  converge vers un réel $\ell$ si, et seulement si,  la suite $\left( u_n \right) - \ell $ est convergente vers un 0.
\end{enumerate}

\textcolor{blue}{\subsection{Limites d'une suite géométrique}}
\textcolor{teal}{Théorème : }
Soit $q$ un nombre réel :
\begin{itemize}
	\item Si $-1<q<1$ alors la suite géométrique de terme général $q^n$ converge vers 0 : $\displaystyle \lim_{n\to +\infty}q^n=0$.	
	\item Si $q>1$ alors la suite géométrique de terme général $q^n$ a pour limite $+\infty$ : $\displaystyle \lim_{n\to +\infty}q^n=+\infty$.
	\item Si $q<-1$ alors la suite géométrique de terme général $q^n$ n'admet pas de limite finie ou infinie.	
\end{itemize}
\textcolor{teal}{Corollaire : }
Soit  $\left( u_n \right)$ une suite géométrique de premier terme $u_0$ non nul et de raison $q$ \textbf{strictement positive}. 
\begin{itemize}
	\item Si $0 < q < 1$ alors la suite $\left( u_n \right)$  converge et $\displaystyle \lim_{n\to +\infty}u_n= 0$.
	\item Si $q = 1$ alors la suite $\left( u_n \right)$ est constante  et égale à  $u_0$.
	\item Si $q > 1$ alors la suite $\left( u_n \right)$ est divergente et admet une limite infinie avec :
	\[\displaystyle \lim_{n\to +\infty}u_n=-\infty \: \text{ si } \: u_0 < 0 \qquad \text{ et } \qquad \displaystyle \lim_{n\to +\infty}u_n=+\infty \: \text{ si } \: u_0 > 0\]
\end{itemize}
\end{document}