\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}
\newcommand{\rep}[1]{\textcolor{blue}{#1}}
\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbf{R}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother


\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}

\newtheorem{preuve}{Preuve}



% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{teal}{}}


\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Suites}
\author{\hfill TES}
\date{}

\begin{document}


\maketitle\\

\section*{Généralités}

Une suite numérique $u$ est une fonction de $\mathbb{N}$ dans $\mathbb{R}$, c'est-à-dire une fonction qui à tout entier naturel $n$ associe un réel, noté $u(n)$ ou, plus généralement, $u_n$.

Soit $f$ une fonction définie au moins sur $\mathbb{R}^+$. Une suite $(u_n)$, telle que $u_n=f(n)$ est dite définie de manière \emph{explicite}.

Soit $f$ une fonction définie sur un intervalle $I$ telle que $f(I)\subset I$.\\
Une suite $(u_n)$ définie par $u_0$ et $u_{n+1}=f(u_n)$ est dite définie \emph{par récurrence}.

\subsection*{Monotonie}
  Une suite est dite :
\begin{itemize}
	\item \emph{croissante} si, pour tout $n$, $u_{n+1}\geqslant u_n$ ;
	\item \emph{décroissante} si, pour tout $n$, $u_{n+1}\leqslant u_n$ ;
	\item \emph{stationnaire} si, pour tout $n$, $u_{n+1}=u_n$.
\end{itemize}
Si la suite ne change pas de sens de variation, on dit qu'elle est \emph{monotone}.\\

\textbf{Méthodes : }\\
\textit{Signe de la différence $u_{n+1}-u_n$ :\\}
\noindent Pour étudier le sens de variation d'une suite, on peut étudier le
signe de la différence $u_{n+1}-u_n$. Cette méthode est très
générale et \og fonctionne \fg{} souvent.\\

\textit{Comparaison de $\frac{u_{n+1}}{u_n}$ à $1$ : \\}
\noindent Pour étudier le sens de variation d'une suite \emph{à termes strictement
positifs}, on peut comparer $\frac{u_{n+1}}{u_n}$ à $1$. \\

Théorème : 
Soit $(u_n)$ la suite définie par la relation $u_n=f(n)$.

Si la fonction $f$ est monotone sur $[0\,;\,+\infty[$, alors la suite $(u_n)$ est monotone et a même sens de variation que $f$.
\section*{Suite arithmétique}

On appelle \emph{suite arithmétique} toute suite $(u_n)$ telle que \[\text{Pour tout entier naturel $n$, }u_{n+1}=u_n+r \text{ où } r\in\mathbb{R}\]
$r$ est appelé \emph{la raison} de la suite arithmétique.

\textbf{Démontrer qu'une suite est arithmétique\\}
Pour démontrer qu'une suite est arithmétique, il suffit de montrer que, pour tout $n$, la différence $u_{n+1}-u_n$ est constante. Cette constante sera alors la raison de la suite.\\
Soit $(u_n)$ une suite arithmétique de premier terme $u_0$ et raison $r$. Alors, pour tout entier naturel $n$, on a : $u_n=u_0+nr$\\

\textbf{Sens de variation\\}
Soit $(u_n)$ une suite arithmétique de raison $r$. Alors :
\begin{itemize}
	\item si $r>0$, $(u_n)$ est strictement croissante ;
	\item si $r<0$, $(u_n)$ est strictement décroissante ;
	\item si $r=0$, $(u_n)$ est constante.\\
\end{itemize}

\textbf{Somme de termes consécutifs : \\}
Soit $(u_n)$ une suite arithmétique et $n$ un entier naturel. Alors :
\[ S_n=u_0+u_1+\ldots+u_n=\frac{(u_0+u_n)(n+1)}{2}\]

\section*{Suite géométrique}
On appelle \emph{suite géométrique} toute suite $(u_n)$ telle que \[\text{Pour tout entier naturel $n$, }u_{n+1}=q\times u_n \text{ où } q\in\mathbb{R}\]
$q$ est appelé \emph{la raison} de la suite géométrique.\\

Soit $(u_n)$ une suite géométrique de premier terme $u_0$ et raison $q$. Alors, pour tout entier naturel $n$, on a :
\[u_n=u_0\times q^n\]
\textbf{Démontrer qu'une suite est géométrique\\}
Pour démontrer qu'une suite est géométrique, il suffit de montrer que, pour tout $n$, le quotient $\dfrac{u_{n+1}}{u_n}$ est constant. Cette constante sera alors la raison de la suite.\\
Soit $(u_n)$ une suite géométrique de premier terme $u_0$ et de raison $q$. Alors pour tout entier naturel $n$, on a : $u_n=u_0 \times q^n$.\\

\textbf{Sens de variation\\}
Soit $(u_n)$ une suite géométrique de raison $q$ telle que $q>0$, $q\neq 0$ et $q\neq 1$. Alors, si $u_0>0$ :
\begin{itemize}
	\item si $0<q<1$, $(u_n)$ est strictement décroissante ;
	\item si $q>1$, $(u_n)$ est strictement croissante ;
	\item si $q=1$, $(u_n)$ est constante.\\
\end{itemize}

\textbf{Somme de termes consécutifs : \\}
Soit $(u_n)$ une suite géométrique de raison $q\neq1$ et $n$ un entier naturel. Alors :
\[ S_n=u_0+u_1+\ldots+u_n=u_0\frac{1-q^{n+1}}{1-q}\]

\hrulefill\\
\textbf{Exercice 1 :}\\
Pour chacune des suites données ci-dessous, o\`u $n\in\mathbb{N}$ :
\vspace{-1em}\begin{multicols}{3}
\begin{itemize}
	\item $u_0=0$ et $u_{n+1}=u_n+\frac{1}{2}$ ;
	\item $v_n=5-2n$ ;
	\item $w_n=(n+1)^2-n^2$ ;
	\item $x_n=\frac{3^n}{n+1}$ ;
	\item $y_n=\left(\frac{1}{2}\right)^n$.
\end{itemize}
\end{multicols}\vspace{-1em}

\begin{enumerate}
	\item Calculer les trois premiers termes.
	\item La suite est-elle géométrique ? arithmétique ?
	\item Si elle est arithmétique ou géométrique :

\begin{enumerate}
	\item calculer le terme de rang 100 ;
	\item calculer la somme des termes jusqu'au rang 100.
\end{enumerate}
\end{enumerate}


\textbf{Exercice 2 : }\\
La suite $(u_n)$ est arithmétique de raison $r=8$. On sait que $u_{100}=650$. \\
Que vaut $u_0$ ?\\

\textbf{Exercice 3 : }\\
La suite $(u_n)$ est arithmétique de raison $r$. On sait que $u_{50}=406$ et $u_{100}=806$.

			\begin{enumerate}
				\item Déterminer $r$ et $u_0$.
				\item Calculer $S=u_{50}+u_{51}+\ldots+u_{100}$.
			\end{enumerate}


\textbf{Exercice 4 : }\\
On considère une suite géométrique $(u_n)$ de premier terme $u_1=1$ et de raison $q=-2$.
\begin{enumerate}
\item Calculer $u_2$, $u_3$ et $u_4$.
	\item Calculer $u_{20}$.
	\item Calculer la somme $S=u_1+u_2+\ldots+u_{20}$.
\end{enumerate}


\textbf{Exercice 5 : }\\
 La suite $(u_n)$ est géométrique de raison $q=\frac{1}{2}$. On sait que $u_{8}=-1$.\\
  Que vaut $u_0$ ?\\
 
\textbf{Exercice 6 : } La suite $(u_n)$ est géométrique de raison $q$. On sait que $u_{4}=10$ et $u_{6}=20$.
	\begin{enumerate}
				\item Déterminer $q$ et $u_0$.
				\item Calculer $S=u_{50}+u_{51}+\ldots+u_{100}$.

\end{enumerate}


\textbf{Exercice 7 : }\\
On donne la suite : $(w_n) : \left\{\begin{array}{l} w_0=0 \\ w_{n+1}=\frac{2w_n+3}{w_n+4} \end{array} \right.$\\
On pose $v_n=\frac{w_n-1}{w_n+3}$
				\begin{enumerate}
					\item Montrer que $(v_n)$ est géométrique. Préciser son premier terme et sa raison.
					\item Donner l'expression de $v_n$ en fonction de $n$.
					\item En déduire l'expression de $w_n$ en fonction de $n$.
					\item En déduire la valeur exacte de $w_{20}$.\\
				\end{enumerate}
				
\textbf{Exercice 8 : }\\
On considère les deux suites $(u_n)$ et $(v_n)$ définies, pour tout $n\in\mathbb{N}$, par :
\begin{center}
$u_n=\frac{3\times 2^n-4n+3}{2}$  et  $v_n=\frac{3\times 2^n+4n-3}{2}$
\end{center}
\begin{enumerate}
	\item Soit $(w_n)$ la suite définie par $w_n=u_n+v_n$.
	 Démontrer que la suite $(w_n)$ est une suite géométrique.
	\item Soit $(t_n)$ la suite définie par $t_n=u_n-v_n$. 
	Démontrer que $(t_n)$ est une suite arithmétique.
	\item Exprimer la somme suivante en fonction de $n$ :
	$S_n=u_0+u_1+\ldots+u_n$
\end{enumerate}

\newpage 
\begin{center}
\color{red}{{\LARGE Correction des exercices} }
\end{center}
\color{blue}
Exercice 1 : \\
Considérons la suite $(u_n)$:
\begin{enumerate}
\item $u_0=0 ; u_1=0,5; u_2=1$
\item On passe d'un terme à l'autre en ajoutant 0,5, c'est une suite arithmétique de raison 0,5 et de premier terme 0.
\item $u_n=0+0,5n$ 
\begin{enumerate}
\item $u_{100}=50$.
\item $S_{100}=\dfrac{(0+50)(100+1)}{2}=2 525$\\
\end{enumerate} 
\end{enumerate}

Considérons la suite $(v_n)$:
\begin{enumerate}
\item $v_0=5 ; v_1=3; v_2=1$
\item On passe d'un terme à l'autre en ajoutant $-2$, c'est une suite arithmétique de raison $-2$ et de premier terme 5.
\item $v_n=5-2n$ 
\begin{enumerate}
\item $v_{100}=-195$.
\item $S_{100}=\dfrac{(5-195)(100+1)}{2}=-9595$\\
\end{enumerate} 
\end{enumerate}

Considérons la suite $(w_n)$:\\
Développons : $w_n=2n+1=1+2n$
\begin{enumerate}
\item $w_0=1 ; w_1=3; w_2=5$
\item On passe d'un terme à l'autre en ajoutant $2$, c'est une suite arithmétique de raison $2$ et de premier terme 1.
\item $w_n=1+2n$ 
\begin{enumerate}
\item $w_{100}=201$.
\item $S_{100}=\dfrac{(1+201)(100+1)}{2}=10201$\\
\end{enumerate} 
\end{enumerate}

Considérons la suite $(x_n)$:
\begin{enumerate}
\item $x_0=1; x_1= 1,5; x_2=3$
\item $x_2-x_1=1,5$ et $x_1-x_0=0,5$ \hfill Ce n'est pas une suite arithmétique.\\

$\dfrac{x_2}{x_1}=2$ et $\dfrac{x_1}{x_0}=1,5$ \hfill Ce n'est pas une suite géométrique.\\
\end{enumerate}

Considérons la suite $(y_n)$: 
\begin{enumerate}
\item $y_0=1;y_1=0,5;y_2=0,25$
\item On passe d'un terme à l'autre en multipliant par $\dfrac{1}{2}$, c'est une suite géométrique de raison $\dfrac{1}{2}$ et de premier terme 1.
\item $y_n=1\times \left( \dfrac{1}{2}\right) ^2$
\begin{enumerate}
\item $y_{100}=\left( \dfrac{1}{2}\right) ^{100}$
\item $S=1 \times \dfrac{1-\left( \dfrac{1}{2}\right) ^{101}}{1-0,5}=2 -2\left( \dfrac{1}{2}\right) ^{101}$
\end{enumerate}
\end{enumerate}

Exercice 2 : \\
$u_n=u_0+nr$\\

$u_{100}=u_0+100\times 8$\\

$650=u_0+800$\\

$u_0=-150$\\

Exercice 3 : \\
\begin{enumerate}
\item Calculons $r$ : \\
$u_{100}=u_{50}+(100-50)r$\\

$806=406+50r$\\

$\dfrac{806-406}{50}=r$\\

$r=8$\\
Calculons $u_0$ :\\
$u_{100}=u_{0}+(100-0)r$\\

$806=u_0+100\times 8$\\

$u_0=6$
\item $S=\dfrac{(premier + dernier)\times \textit{nombre de termes}}{2}$\\ 

$S=\dfrac{(u_{50}+u_{100})\times 51}{2}$\\

$S=30906$
\end{enumerate}

Exercice 4 : \\
\begin{enumerate}
\item $u_2=-2; u_3=4; u_4=-8$
\item $u_n=1 \times (-2)^n$\\
$u_{20}=-524288$
\item $S=premier \times \dfrac{1-q^{\textit{nombre de termes}}}{1-q}$\\ 

$S=1\times \dfrac{1-(-2)^{20}}{1-(-2)}=\dfrac{-1048575}{3}=-349525$
\end{enumerate}


Exercice 5 : \\
$u_n=u_0 \times q^n$\\
$u_8=u_0 \times (0,5)^8$\\
$-1=u_0\times (0,5)^8$\\
$u_0=-256$\\

Exercice 6 : \\
\begin{enumerate}
\item Calculons $q$ : \\
$u_6=u_4 \times q^{6-4}$\\
$20=10q^2$\\

$q=\sqrt{\dfrac{20}{10}}=\sqrt{2}$\\

Calculons $u_0$ :\\
$u_4=u_0 \times (\sqrt{2})^4$\\
$10=u_0\times 4$\\
$u_0=2,5$

\item $S=u_{50} \dfrac{1-(\sqrt{2})^{51}}{1-\sqrt{2}}$\\
Calculons $u_{50}$ : $u_{50}=2,5 \times (\sqrt{2})^{50}$
\end{enumerate}

Exercice 7: 
\begin{enumerate}
\item Calculons $v_{n+1}$ :\\

$v_{n+1}=\dfrac{w_{n+1}-1}{w_{n+1}+3}$\bigskip 

$v_{n+1}=\dfrac{\dfrac{2w_n+3}{w_n+4}-1}{\dfrac{2w_n+3}{w_n+4}+3}=\dfrac{\dfrac{2w_n+3-w_n-4}{w_n+4}}{\dfrac{2w_n+3+3w_n+12}{w_n+4}}= \dfrac{\dfrac{w_n-1}{w_n+4}}{\dfrac{5w_n+15}{w_n+4}}=\dfrac{w_n-1}{5w_n+15}$\bigskip 

$v_{n+1}=\dfrac{w_n-1}{5(w_n+3)}=\dfrac{1}{5}\dfrac{w_n-1}{(w_n+3)}\dfrac{1}{5}v_n$\\

La suite est géométrique de raison $\dfrac{1}{5}$ et de premier terme $v_0$.\\
Calculons $v_0$ : $v_0=-\dfrac{1}{3}$
\item $v_n= -\dfrac{1}{3}\times \left( \dfrac{1}{5}\right) ^n$
\item $-\dfrac{1}{3}\left( \dfrac{1}{5}\right) ^n=\dfrac{w_n-1}{w_n+3}$\\
$-\dfrac{1}{3}\left( \dfrac{1}{5}\right) ^n \times (w_n+3)=w_n-1$\\
$-\dfrac{1}{3}\left( \dfrac{1}{5}\right) ^n \times w_n-\dfrac{1}{3}\left( \dfrac{1}{5}\right) ^n \times 3=w_n-1$\\
$-\dfrac{1}{3}\left( \dfrac{1}{5}\right) ^n \times w_n-w_n=\dfrac{1}{3}\left( \dfrac{1}{5}\right) ^n \times 3-1$\\
$w_n \times \left( -\dfrac{1}{3}\left( \dfrac{1}{5}\right) ^n-1\right) =\left( \dfrac{1}{5}\right) ^n -1$\\
$w_n =\dfrac{\left( \dfrac{1}{5}\right) ^n -1}{-\dfrac{1}{3}\left( \dfrac{1}{5}\right) ^n-1}$\\
\item $w_{20}= \dfrac{\left( \dfrac{1}{5}\right) ^{20} -1}{-\dfrac{1}{3}\left( \dfrac{1}{5}\right) ^{20}-1}$
\end{enumerate}

Exercice 8 : 
\begin{enumerate}
\item Calculons $w_n$ : \\
$w_n= \dfrac{3 \times 2^n-4n+3\times 2^n+4n-3}{2}= 3\times 2^n$\\
C'est une suite géométrique de raison 2 et de premier terme 3.
\item Calculons $t_n$ : \\
$t_n=\dfrac{3 \times 2^n -4n+3-3\times 2^n -4n+3}{2}=-4n+3=3-4n$\\
C'est une suite arithmétique de raison $-4$ et de premier terme 3.
\item On remarque que $w_n+t_n= 2u_n$\\
Donc $Su_n= \dfrac{w_n+t_n}{2}$\
On obtient $S_n= \dfrac{w_0+t_0}{2}+\dfrac{w_1+t_1}{2}+....+\dfrac{w_n+t_n}{2}$\\

$S_n= \dfrac{1}{2}(w_0+t_0+w_1+t_1+...w_n+t_n)$\\

$S_n= \dfrac{1}{2}(w_0+w_1+...+w_n+t_0+t_1+...+t_n)$\\

$S_n= \dfrac{1}{2}\left( w_0\dfrac{1-2^{n+1}}{1-2}+\dfrac{(t_0+t_n)(n+1)}{2}\right) $\\

$S_n=\dfrac{1}{2}(-3(1-2^{n+1})+(3-2n)(n+1))$


\end{enumerate}

\end{document}