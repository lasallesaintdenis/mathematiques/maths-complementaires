\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}
\usepackage{pstricks,pst-plot}
\usepackage{pstricks-add}
\usepackage{sectsty}

\usepackage{graphicx}
\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{theoreme}{Théorème}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textsf{\textsc{\small{Démonstration}}}}
\newtheorem{exemple}{\textsf{\textsc{\small{Exemple}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}
\newtheorem{preuve}{Preuve}



% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{violet}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}

\title{Calcul intégral}
\author{TES}
\date{}

\begin{document}


\maketitle\\
\section{Primitives}
\begin{definition} Soit $F$ une fonction de dérivée $f$ alors $F$ est appelée \textcolor{red}{primitive}  de $f$
\end{definition}

\begin{definition} 
Soit $f$ une fonction définie sur un intervalle $I$, on appelle \textcolor{red}{primitive} de $f$ toute fonction $F$ dérivable sur $I$ telle que $F'=f$ sur l'intervalle $I$.
\end{definition}

\begin{theoreme} 
Toute fonction continue sur un intervalle $I$ admet des primitives sur l'intervalle $I$.
\end{theoreme}

\begin{theoreme} 
Soit $F$ une primitive de $f$ sur l'intervalle $I$ alors $G$ est une primitive de $f$ sur l'intervalle $I$ si et seulement si $G=F+k$, où $k\in \R$ pour tout $x\in \R$
\end{theoreme}

\begin{propriete}
Soit une fonction continue sur l'intervalle $I$. Soient $x_0 \in \R$ et $y_0 \in\R$. \\
Il existe une unique primitive $F$  de $f$ sur l'intervalle $I$ telle que $F(x_0)=y_0$.
\end{propriete}

\begin{exemple}
Soit $f$ la fonction définie sur $\R$ par $f(x)=3x^2+2x+1$.\\
 Déterminer \textbf{la} primitive $F$ de $f$ sur $\R$ telle que $F(1)=-1$.
 
\begin{itemize}
	\item On remarque que $G(x)=x^3+x^2+x$ est \textbf{une} primitive de $f$ sur $\R$.
	\item On sait que toutes les primitives de $f$ sont de la forme $F(x)=x^3+x^2+x+k$, où $k\in\R$.
	\item On cherche $k$ tel que $F(1)=-1$.\\
	 Or $F(1)=1^3+1^2+1+k=k+3$. \\
	 Donc $-1=k+3 \Leftrightarrow k=-4$\\
	Donc $F(x)=x^3+x^2+x-4$ est \textbf{la} primitive de $f$ sur $\R$ telle que $F(1)=-1$.
\end{itemize}
\end{exemple}
\subsection{Primitives des fonctions usuelles}
\begin{center}
\begin{tabular}{|*{2}{>{\centering}m{6cm}|}c|}\hline
Fonction $f$ &	Fonction primitive	($k\in\R$ constante) 	&   Intervalle $I$ \tabularnewline \hline\hline
$f(x)=m$ (constante) & $F(x)=mx+k$ & $\R$ \tabularnewline \hline\hline
$f(x)=x$	& $F(x)=\dfrac{1}{2}x^2+k$ & $\R$ \tabularnewline \hline
$f(x)=x^2$ & $F(x)=\dfrac{1}{3}x^{3}+k$ & $\R$ \tabularnewline \hline
$f(x)=x^3$ & $F(x)=\dfrac{1}{4}x^{4}+k$ & $\R$ \tabularnewline \hline
Plus généralement : $f(x)=x^n$ où $n\in\N$ & $F(x)=\dfrac{1}{n+1}x^{n+1}+k$ & $\R$ \tabularnewline \hline\hline
$f(x)=\dfrac{1}{x^2}$ & $F(x)=-\dfrac{1}{x}+k$ & $]-\infty\,;\,0[$ ou $]0\,;\,+\infty[$ \tabularnewline \hline
$f(x)=\dfrac{1}{x^3}$ & $F(x)=-\dfrac{1}{2}\dfrac{1}{x^{2}}+k$ & $]-\infty\,;\,0[$ ou $]0\,;\,+\infty[$ \tabularnewline \hline
Plus généralement : $f(x)=\dfrac{1}{x^n}$ où $n\in\N$ et $n\geqslant 2$ & $F(x)=-\dfrac{1}{n-1}\dfrac{1}{x^{n-1}}+k$ & $]-\infty\,;\,0[$ ou $]0\,;\,+\infty[$ \tabularnewline \hline\hline
$f(x)=\dfrac{1}{\sqrt{x}}$ & $F(x)=2\sqrt{x}+k$ & $]0\,;\,+\infty[$ \tabularnewline \hline \hline
$f(x)=\dfrac{1}{x}$ & $F(x)=\ln(x)+k$ & $]0\,;\,+\infty[$ \tabularnewline \hline \hline
$f(x)=e^x$ & $F(x)=e^x+k$ & $\R$ \tabularnewline \hline
\end{tabular}\end{center}
\subsection{Opérations sur les primitives}
 Soient $u$ et $v$ deux fonctions continues sur un intervalle $I$. On a alors les propriétés résumées dans le tableau ci-dessous. %\ref{opeprimtab} \vpageref{opeprimtab}. 
 Là encore, les résultats de ce tableau s'obtiennent en vérifiant qu'on a bien $F'=f$ sur l'intervalle considéré.


\begin{tabular}{|*{3}{>{\centering}m{5cm}|}}\hline
Conditions & La fonction s'écrivant sous la forme & admet comme primitive \tabularnewline \hline\hline
& $u'+v'$ & $u+v$ \tabularnewline \hline\hline
Soit $k\in\R$ une constante & $ku'$ & $ku$ \tabularnewline \hline \hline
 & $u'e^u$ & $e^u$ \tabularnewline \hline
\end{tabular}
\pagebreak
\section{Integrale d'une fonction}
\begin{definition} [Aire sous la courbe d'une fonction positive]
Soit $f$ une fonction définie, continue et positive sur un intervalle $[a;b]$ et $\mathcal{C}_f$ sa courbe représentative dans le repère orthogonal $(O;\vec{i};\vec{j})$.\\
L'integrale de $f$ entre $a$ et $b$ est l'aire, du domaine $\mathcal{D}_f$compris entre la courbe $\mathcal{C}_f$, l'axe des abscisses et les droites d'équations $x=a$ et $x=b$.
Ce  nomvre est noté $\int_{a}^{b}f(x)d x$

\end{definition}

\begin{remarque} \hfill \\
\begin{itemize}
 \item $\int_{a}^{b}f(x)d x$ se lit \og intégrale de $a$ à  $b$ de $f(x)d x$ \fg{} ou encore   \og somme de $a$ à  $b$ de $f(x)d x$ \fg .
 \item Les réels $a$ et $b$ sont appelés les bornes de l'intégrale $\int_{a}^{b}f(x)d x$.
\item La variable $x$ est dite \og muette \fg{}, elle n'intervient pas dans le résultat. C'est à  dire qu'on peut la remplacer par n'importe quelle autre variable distincte des lettres $a$ et $b$ :$\int_{a}^{b}f(x)d x =\int_{a}^{b}f(t)d t =\int_{a}^{b}f(u)d u$
\item $\int_{a}^{a}f(x)d x = 0$, car le domaine $\mathcal{D}_f$ est alors réduit à  un segment.
 \end{itemize} 
\end{remarque}

\begin{definition}
Soit $f$ une fonction continue sur un intervalle $I$. Soient $a$ et $b$ deux réels de $I$. \\
Soit $F$ une primitive de $f$.\\
On appelle \textcolor{red}{intégrale de $a$ à $b$ de $f$} le nombre réel, noté $\int_{a}^{b}f(x)dx$, égal à $F(b)-F(a)$. Ainsi :
\[\int_{a}^{b}f(x)dx=F(b)-F(a)\]
On dit que $a$ et $b$ sont les bornes de l'intégrale.
\end{definition}
On admettra que dans le cas d'une fonction $f$ positive avec $a\leqslant b$, donc lorsque l'int\'egrale est \'egale \`a l'aire sous la courbe de $f$, cette aire est bien donn\'ee par $F(b)-F(a)$.

\begin{remarque}
\begin{itemize}
	\item On note aussi : $F(b)-F(a)=\Bigl[F(t)\Bigr]_{a}^{b}$.
	\item Le choix de la primitive $F$ n'influe pas sur la valeur de l'intégrale. En effet, si on prend à la place une primitive $G=F+k$, on a $G(b)-G(a)=F(b)+k-\left(F(a)+k\right)=F(b)-F(a)$.
	\item Dans les cas o\`u $f$ n'est pas toujours positive ou bien quand $a\geqslant b$, l'int\'egrale n'est pas l'aire sous la courbe mais est une quantit\'e math\'ematique (qui n'est pas forc\'ement positive).
\end{itemize}
\end{remarque}


\begin{propriete}
Soit $f$ une fonction continue sur un intervalle $I$ et $a$ et $b$ deux réels de $I$. Alors
\[\int_{a}^{a}f(t)dt=0\]
\end{propriete}

\textbf{Interpr\'etation graphique} : dans le cas d'une fonction positive, $\int_{a}^{a}f(t)dt$ peut \^etre vue comme l'aire d'un rectangle de hauteur $f(t)$ et de base $a-a=0$, donc son aire est \'egale \`a z\'ero.

\begin{propriete}
 Soit $f$ une fonction continue sur un intervalle $I$ et $a$ et $b$ deux réels de $I$. Alors \[\int_{b}^{a}f(t)dt=-\int_{a}^{b}f(t)dt\]
\end{propriete}

\begin{propriete}[Linéarité]
Soient $f$ et $g$ deux fonctions continues sur un intervalle $I$. Soient $a$ et $b$ deux réels de $I$ et $\alpha$ et $\beta$ deux réels quelconques. Alors :
\begin{itemize}
\item $\int_{a}^{b}\left(\alpha f(t)\right)dt=\alpha \int_{a}^{b}f(t)dt$\\
	\item $\int_{a}^{b} \left(f(t)+g(t)\right)dt=\int_{a}^{b}f(t)dt+\int_{a}^{b}g(t)dt$\\
	\item Plus généralement :\\ $\int_{a}^{b} \left(\alpha f(t)+ \beta g(t)\right)dt=\alpha \int_{a}^{b}f(t)dt+\beta \int_{a}^{b}g(t)dt$
\end{itemize}
\end{propriete}

\textbf{Interpr\'etation graphique} : dans le cas de fonctions positives, avec $a\leqslant b$ et $\alpha$ et $\beta$ positifs, on peut interpr\'eter ces propri\'et\'es de la fa\c{c}on suivante : 
\begin{itemize}
 \item l'aire sous la courbe de $\alpha f$ est \'egale \`a $\alpha$ fois l'aire sous la courbe de $f$ ;
 \item l'aire sous la courbe de $f+g$ est \'egale \`a la somme de l'aire sous la courbe de $f$ et de l'aire sous la courbe de $g$.
\end{itemize}

\begin{propriete}[Inégalités]
Soient $f$ et $g$ deux fonctions continues sur $I$ et $a\leqslant b$ deux réels de $I$.

\begin{itemize}
	\item Si $f\geqslant 0$ sur $I$ alors $\int_{a}^{b}f(t)dt\geqslant 0$.
	\item Si $f\leqslant g$ sur $I$ alors $\int_{a}^{b}f(t)dt\leqslant \int_{a}^{b}g(t)dt$.
\end{itemize}
\end{propriete}


\textbf{Interpr\'etation graphique} : dans le cas de fonctions positives, comme $a\leqslant b$, on peut interpr\'eter ces propri\'et\'es de la fa\c{c}on suivante : si $f\leqslant g$ alors l'aire sous la courbe de $f$ est inf\'erieure \`a l'aire sous la courbe de $g$, le premier point \'etant un cas particulier de ce cas g\'en\'eral (l'aire sous la courbe de la fonction constante \'egale \`a z\'ero est \'egale \`a z\'ero).

\begin{propriete}[Relation de \textsc{Chasles}]
Soit $f$ une fonction continue sur un intervalle $I$ et $a$, $b$ et $c$ trois réels de $I$. Alors
\[\int_{a}^{b}f(t)dt=\int_{a}^{c}f(t)dt+\int_{c}^{b}f(t)dt\]
\end{propriete}
\subsection{Intégrale et primitive}
\begin{theoreme}
 Soit $f$ une fonction continue sur un intervalle $I=[a\,;\,b]$. Alors la fonction $F$ d\'efinie sur $[a\,;\,b]$ par $F(x)=\int_{a}^{x}f(t)dt$ est d\'erivable sur $[a\,;\,b]$ et a pour d\'eriv\'ee $f$.
\end{theoreme}


\begin{definition}
La valeur moyenne d'une fonction $f$ continue sur un intervalle $[a\,;\,b]$ est le nombre : \[\mu=\dfrac{1}{b-a}\int_{a}^{b}f(t)dt\]
\end{definition}

\end{document}