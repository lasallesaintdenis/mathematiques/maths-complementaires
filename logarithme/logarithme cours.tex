\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbb{R}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textsf{\textsc{\small{Démonstration}}}}
\newtheorem{exemple}{\textsf{\textsc{\small{Exemple}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}
\newtheorem{preuve}{Preuve}



% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{violet}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}

\title{Logarithme Népérien}
\author{TES}
\date{2015-2016}

\begin{document}

\maketitle\\
\section{Fonction logarithme népérien}
La fonction exponentielle est continue, strictement croissante et pour tout réel $x$, $e^x \in \left] 0; +\infty \right[$.

D'après le théorème de la valeur intermédiaire, pour tout réel $a > 0$, l'équation $e^x = a$ admet une unique solution, c'est à  dire que :

\begin{center}
pour tout réel $a$ strictement positif, il existe un unique réel $x$ tel que $e^x =a$
\end{center}

On définit une nouvelle fonction appelée logarithme népérien qui à tout réel strictement positif, associe son unique antécédent par la fonction exponentielle.


On dit que la fonction logarithme népérien est la fonction réciproque de la fonction exponentielle.

	Dans un repère orthonormé, leurs courbes représentatives sont symétriques par rapport Ã  la droite $\mathcal{D}$ d'équation $y=x$.
	
\begin{tikzpicture}[samples=200]
\draw[-stealth] (-3,0)--(5,0);
\draw[-stealth] (0,-3)--(0,5);
\draw [red, domain=-3:1.5] plot  (\x,{exp(\x) });
\draw [red, domain=0.1:5] plot  (\x,{ln(\x) });
\draw [blue, dashed,domain=-3:5] plot  (\x,{(\x) });
\end{tikzpicture}

\subsection{Définition}
La fonction logarithme népérien, notée $\ln$, est la fonction définie sur $\left] 0; +\infty \right[$ qui à tout réel $x$ strictement positif, associe le réel $y$ tel que $e^y =x$.
\[ x>0 \text{ et } y=\ln (x) \:\text{ équivaut à  }\: x=e^y \]

\begin{remarque}\hfill\\

\begin{itemize}
	\item  On note $\ln x$, au lieu de $ln(x)$, le logarithme népérien de $x$, lorsqu'il n'y a pas d'ambiguïté.
	\item  $e^0 = 1$ donc $\ln(1)=0$.
	\item  $e^1 = e$ donc $\ln(e)=1$.
	\item  Pour tout réel $a>0$, l'équation $e^x= a$ a pour unique solution $x=\ln a$.
\end{itemize}
\end{remarque}

Conséquences : 

\begin{enumerate}
	\item Pour tout réel $x$ strictement positif, $e^{\ln x}=x$.
	\item Pour tout réel $x$, $\ln\left( e^x\right)=x$.
\end{enumerate}
\begin{demonstration}

\begin{enumerate}
	\item Pour tout réel $x > 0$,  $y=\ln x \Leftrightarrow  e^y = x$ donc $e^{\ln x}=x$.
	\item Pour tout réel $x$, $e^ x=y  \Leftrightarrow \ln(y) = x$ soit $\ln\left( e^x\right)=x$.
\end{enumerate}
\end{demonstration}
\section{Propriétés algébriques}
\subsection{Propriété fondamentale}
Pour tous réels $a$ et $b$ strictement positifs : \[ \ln(a \times b)=\ln(a) + \ln (b) \]
\begin{demonstration}
Soient $a>0$ et $b>0$ deux réels strictement positifs, 
 
 Par définition de la fonction $\ln$  : $a=e^{\ln a}$ ,  $b=e^{\ln b}$  et  $a\times b=e^{\ln \left(a\times b\right)}$
 
 D'autre part, $a \times b = e^{\ln a}\times e^{\ln b} = e^{\ln a + \ln b}$
 
 D'où $e^{\ln \left(a\times b\right)}=e^{\ln a + \ln b}$.  Donc $\ln \left(a\times b\right)=\ln a + \ln b$.
\end{demonstration}
\subsection{Règles de calculs}
Pour tous réels $a$ et $b$ strictement positifs et $n$ entier relatif :
\begin{enumerate}
	\item $\ln\left( \dfrac{1}{a} \right) = - \ln a$
	\item $\ln\left( \dfrac{a}{b} \right) =  \ln a- \ln b$
	\item $\ln\left( a^n \right) = n \ln a$
	\item $\ln\left( \sqrt{a} \right) = \dfrac{1}{2} \ln a$
\end{enumerate}
\begin{demonstration}
\begin{enumerate}
	\item Soit $a>0$ alors $\dfrac{1}{a} > 0$. Or $a \times \dfrac{1}{a} = 1$ donc \[\ln \left( a \times \dfrac{1}{a} \right) = \ln 1 \Leftrightarrow \ln a + \ln \dfrac{1}{a} = 0 \Leftrightarrow  \ln \dfrac{1}{a} =-\ln a \]
	\item Soient $a>0$ et $b > 0$ 
	\[\ln \left( \dfrac{a}{b} \right) = \ln \left( a \times \dfrac{1}{b} \right) =  \ln a + \ln \dfrac{1}{b} = \ln a-\ln b \]
	
	\item Soient $a>0$ un réel strictement positif et $n$ un entier relatif, \[e^{\ln\left( a^n \right)}=a^n \text{ et } e^{n \ln a}=\left( e^{\ln a}\right)^n=a^n\]
	
	Donc  $e^{\ln\left( a^n \right)}= e^{n \ln a}$ et par conséquent, $\ln\left( a^n \right) = n \ln a$.
 \item Soit $a>0$ alors $\left( \sqrt{a} \right) ^2 =a$ donc \[\ln a = \ln  \left( \sqrt{a} \right) ^2  =  2 \ln  \sqrt {a}  \]
	
	\end{enumerate}
\end{demonstration}

\section{\'Etude de la fonction logarithme népérien}
\subsection{Dérivée}
La fonction logarithme népérien est dérivable  sur $]0; +\infty[$ et pour tout réel $x>0$, $\ln ' (x) =\dfrac{1}{x}$.
\begin{demonstration}
On admet que la fonction $\ln$ est dérivable sur $]0; +\infty[$. 

Soit $f$ la fonction définie sur $]0; +\infty[$ par $f(x)=e^{\ln x}$. 

$f$ est dérivable sur $]0; +\infty[$ et pour tout réel $x>0$, $f'(x)= \ln ' (x) \times e^{\ln x}= \ln ' (x) \times x$.

Or pour tout réel $x>0$, $f(x)=x$ d'où $f'(x)=1$

Ainsi pour tout réel $x>0$,  $\ln ' (x) \times x=1$  donc $\ln ' (x) =\dfrac{1}{x}$.
\end{demonstration}
\subsection{Variation}
La fonction logarithme népérien est continue et strictement croissante sur $]0; +\infty[$ .
\begin{demonstration}
 La fonction $\ln$ est dérivable sur $]0; +\infty[$ donc continue sur cet intervalle.
 
 La dérivée de la fonction $\ln$ est la fonction définie sur $]0; +\infty[$ par $\ln '(x)= \dfrac{1}{x}$. Or si $x > 0$ alors, $\dfrac{1}{x} > 0$.
 
La dérivée de la fonction $\ln$ est strictement positive, donc la fonction $\ln$ est strictement croissante sur $]0; +\infty[$.

  \begin{tikzpicture}
      \tkzTabInit[espcl=3.5]
      {$x$ / 1  ,$g$/2}%
      {$-0$ ,$+\infty$ }%
      \tkzTabVar{D- / \ $-\infty$, +/$+\infty$}
    \end{tikzpicture}
\end{demonstration}
\subsubsection{Conséquences}
 
 On déduit de ce théorème les propriétés suivantes :
 
Pour tous réels $a$ et $b$ strictement positifs :
\begin{itemize}
	\item $\ln a = \ln b$ si, et seulement si, $a=b$
	\item $\ln a > \ln b$ si, et seulement si, $a>b$
\end{itemize}

Puisque  $\ln 1 = 0$ :

Pour tout réel $x$ strictement positif :
\begin{itemize}
	\item $\ln x = 0$ si, et seulement si, $x=1$
	\item $\ln x > 0$ si, et seulement si, $x>1$
		\item $\ln x < 0$ si, et seulement si, $0<x<1$
\end{itemize}

Comme la fonction logarithme népérien est continue, strictement croissante et que pour tout réel $x>0$, $\ln x \in \R$ alors, d'après le théorème de la valeur intermédiaire : 

Pour tout réel $k$, l'équation $\ln x = k$ admet dans l'intervalle $]0;+\infty[$ une unique solution $x=e^k$.
\subsection{Courbe représentative}


Notons $\mathcal{C}_{\ln}$ la courbe représentative de de la fonction logarithme népérien.

\begin{itemize}
	\item $\ln (1) =0$ et $\ln (e) =1$ donc les points $A(1;0)$ et $B(e;1)$ appartiennent à la courbe $\mathcal{C}_{\ln}$.
	\item Le coefficient directeur de la tangente à la courbe $\mathcal{C}_{\ln}$ au point $A(1;0)$ est $\ln ' (1)= 1$. 
	
	Donc la tangente à la courbe $\mathcal{C}_{\ln}$ au point $A(1;0)$ a pour équation : $ y= x-1$.
	
		\item Le coefficient directeur de la tangente à la courbe $\mathcal{C}_{\ln}$ au point $B(e;1)$ est $\ln ' (e)= \dfrac{1}{e}$. 
	
	Donc la tangente à la courbe $\mathcal{C}_{\ln}$ au point $B(e;1)$ a pour équation : \[ y= \dfrac {1}{e} (x-e) +1 \Leftrightarrow y= \dfrac {1}{e} x\]
	La tangente à la courbe $\mathcal{C}_{\ln}$ au point d'abscisse $e$ passe par l'origine du repère.
	
	\item Comme la fonction inverse est strictement décroissante sur l'intervalle $]0;+\infty[$, la dérivée de la fonction $\ln$ est strictement décroissante. 
	
	Par conséquent, la fonction $\ln$ est concave sur $]0;+\infty[$.
\end{itemize}
\begin{center}
\begin{tikzpicture}[samples=200]
\draw [-stealth] (-0.5,0)--(8,0);
\draw (0,0) node[below left]{$0$} (1,0) node {+} node[below]{$1$} (2,0)node {+} node[below]{$2$} (3,0) node {+} node[below]{$3$} (4,0) node {+} node[below]{$4$}  (5,0)node {+} node[below]{$5$} (0,1) node{+} node[left]{$1$} (0,2)node{+} node[left]{$2$}(0,3) node{+} node[left]{$3$}(6,0)  node {+} node[below]{$6$}(7,0)  node {+} node[below]{$7$} (8,0) node {+} node[below]{$8$}  (0,-1)node{+} node[left]{$-1$}(0,-2)node{+} node[left]{$-2$};
\draw[gray, dotted] (0,1) -| (2.72,0);
\draw[-stealth] (0,-2.5)--(0,3);
\draw[red,domain=0.1:8] plot  (\x,{ln(\x) });
\draw[blue,domain=0:4] plot(\x,{\x-1});
\draw[green, domain=0:3.5] plot (\x,{0.37*\x});
\end{tikzpicture}
\end{center}
\pagebreak
\section{Synthèse}
Pour tous réels $x,y >0$ et tout entier relatif $n\in \Z$, on a :
\begin{enumerate}
\begin{Large}
\item $ln$ est  une fonction continue et dérivable sur l'intervalle  $]0;+\infty[$
\item $ln$ est  une fonction croissante sur l'intervalle  $]0;+\infty[$
\item $ln$ est concave  sur l'intervalle  $]0;+\infty[$
\item $e^{ln(x)}=x$
\item $ln(e^{a})=a$ avec $a\in \R$
\item $ln(1)=0$
\item $ln(e)=1$
\item $ln(xy)=ln(x)+ln(y)$\\
\item $ln\left( \dfrac{1}{y}\right) =-ln(y)$\\
\item $ln\left( \dfrac{x}{y}\right) =ln(x)-ln(y)$\\
\item $ln(x^{n})=n\times ln(x)$\\
\item $ln(\sqrt{x})=\dfrac{1}{2}ln(x)$\\
\item $ln'(x)=\dfrac{1}{x}$\\
\item $ln'(u)=\dfrac{u'}{u}$
\end{Large}
\end{enumerate}
\end{document}